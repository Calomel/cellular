import fractions
import collections
import unittest

import music21 as M
import numpy as N

import cellular.music.util as util

def _time_unit(cells):
    timestamp = set()
    for c in cells:
        timestamp.add(c.offset)
        timestamp.add(c.duration.quarterLength)
    return util.fract_gcd(list(timestamp)) 

def to_subscores(score):
    scores = []
    for e in score.recurse():
        if isinstance(e, M.stream.Score):
            scores.append(e)
    return scores if len(scores) > 0 else [score]

def matrify(score, maxTimeunitDenom=256):
    _fract = fractions.Fraction

    noteMap = collections.defaultdict(lambda: [])
    offset = 0
    finishingtime = 0
    pitchMin = pitchMax = None
    for el in score.recurse():
        if isinstance(el, M.chord.Chord):
            for p in el.pitches:
                pitchMin = min(pitchMin, p.ps) if pitchMin is not None else p.ps
                pitchMax = max(pitchMax, p.ps) if pitchMax is not None else p.ps
                curNote = M.note.Note(p)
                curNote.offset = el.offset
                curNote.duration.quarterLength = el.duration.quarterLength
                noteMap[offset].append(curNote)
        elif isinstance(el, M.stream.Measure):
            offset = el.offset
            finishingtime = max(finishingtime, offset)
        elif isinstance(el, M.note.Note):
            pitchMin = util.cond_min(pitchMin, el.pitches[0].ps)
            pitchMax = util.cond_max(pitchMax, el.pitches[0].ps)
            noteMap[offset].append(el)

    timeunit = [_time_unit(v) for v in noteMap.values()]
    timeunit = util.fract_gcd(timeunit)
    if timeunit.denominator > maxTimeunitDenom:
        raise Exception(f'Time unit {timeunit} is not a whole fraction')

    endtime = finishingtime
    if finishingtime in noteMap:
        endtime += max(v.offset + v.duration.quarterLength for v in noteMap[finishingtime])
    endtime = _fract(endtime)
    assert endtime % timeunit == 0, \
           f"Endtime {endtime} indivisible by {timeunit}"
    duration = int(endtime / timeunit)

    rangePitch = int(pitchMax - pitchMin + 1) 
    m = N.zeros(shape=(duration, rangePitch), dtype='bool')
    for k, v in noteMap.items():
        assert _fract(k) % timeunit == 0
        tBase = int(_fract(k) / timeunit)
        for n in v:
            p = int(n.pitches[0].ps - pitchMin)
            o = _fract(n.offset)
            assert o % timeunit == 0
            tBegin = tBase + int(o / timeunit)
            ql = _fract(n.duration.quarterLength)
            assert ql % timeunit == 0, \
                   f"Duration {ql} indivisible by {timeunit}"
            tEnd = tBegin +int(ql / timeunit)
            m[tBegin:tEnd,p] = 1
    return m, timeunit, pitchMin

def _stripe(width, low, high):
    assert low <= high
    low = max(low, 0)
    high = min(high, width-1)
    result = N.pad(N.ones((high-low+1,), bool), pad_width=((low,width-high-1),))
    assert result.shape == (width,)
    return result

def envelope(x, radius=1):
    n, nPitchs = x.shape
    def single(i):
        idx, = N.where(x[max(0, i-radius):min(n, i+radius+1),:].sum(axis=0) > 0)
        return (idx.min(), idx.max()) if len(idx) > 0 else None
    idxs = [single(i) for i in range(n)]
    prevIdx = None
    prevLow, prevHigh = None,None
    result = N.zeros(x.shape, bool)
    for i,lims in enumerate(idxs):
        if prevIdx is None:
            if lims is not None:
                prevIdx = i
                prevLow, prevHigh = lims
        elif lims is not None:
            low, high = lims
            # Diagonally interpolate between lims and previous lims.
            
            # Suppose the past frame is at the position
            #
            #    [prevIdx, prevIdx+1] * [prevLow, prevHigh+1]
            #
            # and the current block is at
            #
            #    [i, i+1] * (low, high+1)
            #
            # If low < prevLow, the interpolation of the lower corners is
            #
            #    (prevIdx, prevLow) -- (i, low)
            #
            # If low > prevLow, the interpolation is
            #
            #    (prevIdx+1, prevLow) -- (i+1, low)
            #
            # Both lines have the same slope with the difference that in the first case, the line is shifted
            # left by 1 unit
            flagDown = low < prevLow
            flagUp = high > prevHigh
            
            slopeL = (low - prevLow) / (i - prevIdx)
            slopeH = (high - prevHigh) / (i - prevIdx)
            #print(f"Bridging: ({prevIdx}, {prevLow}, {prevHigh}) -- ({i}, {low}, {high})")
            for j in range(prevIdx, i+1):
                l = N.int(N.floor(prevLow + slopeL * (j - prevIdx - .5 + flagDown)))
                h = N.int(N.ceil(prevHigh + slopeH * (j - prevIdx - .5 + flagUp)))
                
                if j == prevIdx:
                    if not flagDown: l = prevLow
                    if not flagUp: h = prevHigh
                elif j == i:
                    if flagDown: l = low
                    if flagUp: h = high
                #print(f"({j}, {l}, {h}, {prevLow + slopeL * (j - prevIdx - .5 + flagDown)}, {prevHigh + slopeH * (j - prevIdx - .5 + flagUp)})")
                result[j,l:h+1] = 1
            prevIdx = i
            prevLow, prevHigh = lims
            #l = N.int(N.floor(low + slopeL * (i - prevIdx + .5 - flagDown)))
            #h = N.int(N.ceil(high + slopeH * (i - prevIdx + .5 - flagUp)))
            #prevLow, prevHigh = l,h
        
    assert len(result) == n
    return N.stack(result)


def to_cells(score, radius=1):
    """
    Convert a score to a matrix form usable by a cellular automaton.

    Note: To save space, all arrays are stored in boolean form. Further
    conversions may be required.
    """
    mat, minTimeUnit, minPitch = matrify(score)
    env = envelope(mat, radius=radius)
    return N.stack([env, mat])

class TestUtil(unittest.TestCase):

    def test_matrify(self):
        s = M.stream.Score(id='mainScore')
        p0 = M.stream.Part(id='part0')
        p1 = M.stream.Part(id='part1')
        
        m01 = M.stream.Measure(number=1)
        m01.append(M.note.Note('C', type="half"))
        m01.append(M.note.Note('C#', type="half"))
        m02 = M.stream.Measure(number=2)
        m02.append(M.note.Note('D', type="whole"))
        p0.append([m01, m02])
        
        m11 = M.stream.Measure(number=1)
        m11.append(M.note.Note('E', type="whole"))
        m12 = M.stream.Measure(number=2)
        m12.append(M.note.Note('F', type="whole"))
        p1.append([m11, m12])
        
        s.insert(0, p0)
        s.insert(0, p1)
        
        mat, timeunit, minPitch = matrify(s)
        
        truth = N.array([
           # C C# D  E  E# F
           [1, 0, 0, 0, 1, 0],
           [0, 1, 0, 0, 1, 0],
           [0, 0, 1, 0, 0, 1],
           [0, 0, 1, 0, 0, 1],
        ], dtype='bool')
        
        self.assertTrue((mat == truth).all())
        self.assertEqual(timeunit, fractions.Fraction(2, 1))
        self.assertEqual(minPitch, 60)

    def test_stripe(self):
        self.assertTrue(N.array_equal(_stripe(6, 2, 4),
            N.array([0,0,1,1,1,0]).astype(bool)))


if __name__ == '__main__':
    unittest.main()
