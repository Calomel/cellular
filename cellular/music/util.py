import math, fractions
import functools
import unittest

def fract_gcd(x, y=None):
    if y is None:
        x = [fractions.Fraction(f) for f in x]
        return functools.reduce(fract_gcd, x)
    else:
        z_top = math.gcd(x.numerator, y.numerator)
        z_bot = x.denominator // math.gcd(x.denominator, y.denominator) * y.denominator
        return fractions.Fraction(z_top, z_bot)


def cond_min(x, y):
    if x is None:
        return y
    elif y is None:
        return x
    else:
        return min(x,y)
def cond_max(x, y):
    if x is None:
        return y
    elif y is None:
        return x
    else:
        return max(x,y)

class TestUtil(unittest.TestCase):

    def test_fract_gcd(self):
        _fract = fractions.Fraction

        self.assertEqual(   fract_gcd(_fract(2,3), _fract(1,4)), _fract(1, 12))
        self.assertNotEqual(fract_gcd(_fract(2,3), _fract(1,2)), _fract(1, 12))
        self.assertEqual(   fract_gcd(_fract(0,1), _fract(3,4)), _fract(3, 4))

        self.assertEqual(fract_gcd([0, 0.25, 0.75, _fract(1,3)]),
                         _fract(1, 12))
        self.assertEqual(fract_gcd([0, _fract(2,5), _fract(8,7)]),
                         _fract(2, 35))

    def test_cond_minmax(self):
        self.assertEqual(cond_min(1,2), 1)
        self.assertEqual(cond_min(None,2), 2)
        self.assertEqual(cond_min(None,None), None)
        self.assertEqual(cond_min(3,None), 3)
        self.assertEqual(cond_max(1,2), 2)
        self.assertEqual(cond_max(None,2), 2)
        self.assertEqual(cond_max(None,None), None)
        self.assertEqual(cond_max(3,None), 3)

if __name__ == '__main__':
    unittest.main()

