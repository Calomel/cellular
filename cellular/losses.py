import tensorflow as tf
import tensorflow.keras as K

def logistic_loss(s, y_true, y_pred):
    return tf.reduce_mean(K.losses.binary_crossentropy(y_true, y_pred), [-1, -2])

def l2_loss(s, y_true, y_pred):
    return tf.reduce_mean(tf.square(y_true - y_pred), [-2, -3, -1])

def l1_loss(s, y_true, y_pred):
    return tf.reduce_mean(tf.math.abs(y_true - y_pred), [-2, -3, -1])
