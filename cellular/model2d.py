import numpy as N
import tensorflow as tf
import tensorflow.keras as K
import tensorflow.keras.backend as KB
import tensorflow.keras.layers as KL
import tensorflow.keras.models as KM
import tensorflow.keras.regularizers as KR

class WorldConfig:

    """
    By convention, x_ variables contain the substrata and y_ variables do not.
    """
    
    def __init__(self, 
                 width=256,
                 height=None,
                 neighbour = 3,
                 nVisible = 3,
                 nSubstrata = 12,
                 dtype = KB.floatx()):
        """
        (width, height, 1+nVisible+nSubstrata): Dimension of the world.

        neighbour: Cells can only transition from dead to alive if another
                   live cell exists in the vincinity. `neighbour` controls this
                   and should be set to an odd number.

        ties: A list of (int, array)|None, where the array must be of type
            integer and int indicates the axis of the tie (0 or 1). When a tie
            is applied, all cells on the same level are tied to the same
            weight.  For example, a chromatic (using 6 instead of 12 tones) tie
            can be translated to

                [1,2,3,4,5,6,1,2,3,4,5,6]
        
            When the update rule is applied, the value of cell [0] and [6] are
            averaged and then distributed. The length of the array must be
            equal to the number of channels. This can be considered as a forced
            equalisation 
        """
        
        self.width = width
        if height is None:
            self.height = self.width
        else:
            self.height = height
        assert isinstance(self.width, int) and isinstance(self.height, int)
        assert self.width > 1 or self.height > 1
        assert isinstance(nVisible, int) and nVisible >= 0
        assert isinstance(nSubstrata, int) and nSubstrata >= 1
        self.neighbour = 3
        self.nVisible = nVisible
        self.nSubstrata = nSubstrata
        self.nChannels = 1 + nVisible + nSubstrata

        self.dtype = dtype
        
    @property
    def shape(self, visible=False):
        if visible:
            return (self.width, self.height, 1+self.nVisible)
        else:
            return (self.width, self.height, self.nChannels)
        
    def to_visible(self, x):
        return x[..., :self.nVisible+1]
    def to_substrata(self, x):
        return x[..., -self.nSubstrata:]
    
    def to_chroma(self, x):
        return x[..., 1:self.nVisible+1]
    def to_alpha(self, x):
        return tf.clip_by_value(x[...,0], 0.0, 1.0)

    #@staticmethod
    def to_living_mask(self, x, living_thresh=0.1):
        """
        A cell can be set to alive when its neighbours are alive.
        """
        alpha = x[:,:,:,0:1]
        return tf.nn.max_pool2d(alpha, self.neighbour, [1,1,1,1], 'SAME') > living_thresh


    def preprocess_tie(self, t, axis, average=True):
        """
        average: If true, take the mean of the tie. This is used to preserve
        spatial invariance.
        """
        assert axis in {0,1}
        if t is None:
            return N.eye(self.shape[axis], dtype=self.dtype)

        assert t.ndim == 1, "Tie must be 1-dimensional"
        assert t.size == self.shape[axis]
        chromas = N.unique(t)
        # Calculate the distinct masks by separating into chromas
        masks = t[...,N.newaxis] == chromas
        t = masks@masks.T
        if average:
            t = t.astype(self.dtype) / t.sum(axis=-1)
        return t

    def apply_ties(self, x, tiesH=None, tiesV=None):
        if tiesH is not None:
            x = tf.einsum('nijc,nioc->nojc', x, tiesH, name='WorldConfig.tiesH')
        if tiesV is not None:
            x = tf.einsum('nijc,njoc->nioc', x, tiesV, name='WorldConfig.tiesV')
        return x

    @staticmethod
    def create_stripes(n, period, shift):
        shift = shift % period
        # 1. Create periodic stripes
        basis = N.array([(i+shift)%period for i in range(period)])
        t1 = N.tile(basis, int(N.ceil(n / period)) * period)[:n]
        
        # 2. Create aperiodic, ascending stripes
        t2 = N.repeat(range(int(N.ceil(n / period)+1)), period)[shift:n+shift]
        return t1, t2


    def rec_cast(self, x):
        def recursive_cast(x):
            if x is None:
                return tf.constant([0.])
            elif isinstance(x, N.ndarray) and x.dtype == 'object':
                return [recursive_cast(y) for y in x]
            else:
                return tf.cast(x, self.dtype)
        return recursive_cast(x)




class CAModel(K.Model):

    def __init__(self,
                 worldConfig,
                 dmodel=None,
                 fireRate=0.5,
                 addMaskLayer=False,
                 addHintLayer=False,
                 exitStyle=None,
                 name=None,
                 nTieless = 0):
        """
        Refer to https://distill.pub/2020/growing-ca/.
        
        The state cells consist of
        [ alpha | visible | substrata ]
        
        addMaskLayer: If true, the mask is appended to the input.
        addHintLayer: If true, a hint layer is appended.
        exitStyle: None: Nothing, or a function like tf.nn.tanh
        """
        super().__init__()
        self.cfg = worldConfig
        self.fireRate = fireRate

        self.addMaskLayer = addMaskLayer
        self.addHintLayer = addHintLayer
        self.exitStyle = exitStyle

        if dmodel:
            self.dmodel = dmodel
        else:
            self.dmodel = K.Sequential([
                KL.Conv2D(128, 1, activation=tf.nn.relu, name='CAConv1'),
                KL.Conv2D(self.cfg.nChannels, 1, activation=None,
                          kernel_initializer=tf.zeros_initializer(), name='CAConv2'),
            ])

        if name is not None:
            self._name = name
        self.nTieless = nTieless

        # Dummy call to build the model
        mask = tf.zeros([1,3,3], dtype=self.cfg.dtype)
        self(tf.zeros([1, 3, 3, self.cfg.nChannels], dtype=self.cfg.dtype),
             maskLock = mask,
             maskHint = mask[...,tf.newaxis])
        

    @tf.function
    def perceive(self, x, scales=None, angle=0.0):
        """
        scales: Either None or (x.shape[0],3) array corresponding to
                scalings of iden,dx,dy

                To avoid ambiguities the scales array must be 2d.
        """

        # Build the convolution kernel.
        identify = N.float32([0, 1, 0])
        identify = tf.cast(N.outer(identify, identify), self.cfg.dtype)
        dx = N.outer([1, 2, 1], [-1, 0, 1]) / 8.0    # Sobel filter
        dy = dx.T

        c, s = tf.cos(angle), tf.sin(angle)
        dx,dy = c*dx - s*dy, s*dx + c*dy

        dx = tf.cast(dx, self.cfg.dtype)
        dy = tf.cast(dy, self.cfg.dtype)
        kernel = tf.stack([identify, dx, dy], -1)[:, :, None, :]
        kernel = tf.repeat(kernel, self.cfg.nChannels, axis=2)

        z = tf.nn.depthwise_conv2d(x, kernel, [1, 1, 1, 1], 'SAME')

        # Rescale the convolved perception state. 
        if scales is not None:
            # Note that depthwise_conv2d's output channels are ordered by
            # 1. input channel 2. kernel filter
            scales = tf.tile(scales, [1, self.cfg.nChannels])[:,None,None,:]
            z = z * scales
            
        return z

    @tf.function
    def call(self, x,
             fireRate=None, scales=None, angle=0.0, step_size=1.0,
             maskLock = None, maskHint=None, tiesH=None, tiesV=None, **kwargs):
        """
        fireRate: Override the default fire rate given in this model.
        """
        preLifeMask = self.cfg.to_living_mask(x)

        if tiesH is None and tiesV is None:
            xPr = x
        elif self.nTieless > 0:
            xPr = self.cfg.apply_ties(x[...,self.nTieless:], tiesH, tiesV)
            xPr = tf.concat([x[...,:self.nTieless], xPr], axis=-1)
        else:
            xPr = self.cfg.apply_ties(x, tiesH, tiesV)

        z = self.perceive(xPr, scales=scales, angle=angle)
        if self.addMaskLayer:
            assert maskLock is not None
            z = tf.concat([z, maskLock[...,N.newaxis]], axis=-1)

        dx = self.dmodel(z) * step_size
        if fireRate is None:
            fireRate = self.fireRate

        if maskLock is not None:
            # Visible channels are not allowed to change.
            maskLock = tf.cast(maskLock, tf.bool)
            mask0 = tf.zeros_like(maskLock, tf.bool)
            mask = ~tf.stack(
                [maskLock] * (1 + self.cfg.nVisible) + \
                [mask0] * self.cfg.nSubstrata,
                axis=-1
            )
            dx = dx * tf.cast(mask, self.cfg.dtype)

        maskClock = tf.random.uniform(tf.shape(x[:, :, :, :1])) <= fireRate
        dx = dx * tf.cast(maskClock, self.cfg.dtype)

        x += dx

        postLifeMask = self.cfg.to_living_mask(x)
        lifeMask = preLifeMask & postLifeMask
        x *= tf.cast(lifeMask, self.cfg.dtype)

        if self.exitStyle:
            x = self.exitStyle(x)

        return x

    def execute(self, x, iterations, meta, training=True):
        if isinstance(iterations, int):
            iterations = tf.range(0, iterations)

        for _ in iterations:
            x = self(x, training=training, **meta)

        return x


