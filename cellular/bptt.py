import numpy as N
import numpy.random as NR

import abc, sys, unittest
from overrides import final, overrides

class Segment:

    def __init__(self, start, stop, weight=1.):
        self.start = start
        self.stop = stop
        self.weight = weight

    def to_tensor(self):
        import tensorflow as tf
        return tf.range(self.start, self.stop, delta=1)

    def __eq__(self, other):
        return (isinstance(other, self.__class__)
            and self.__dict__ == other.__dict__)

    def __ne__(self, other):
        return not self.__eq__(other)

class Scheduler(metaclass=abc.ABCMeta):

    def __init__(self):
        pass

    @abc.abstractmethod
    def schedule(self, steps):
        pass

class SchedulerEven(Scheduler):

    def __init__(self, parts=1, meanPartSize=None, weightTaper=0.0):
        super().__init__()

        assert isinstance(parts, int) and parts >= 1
        self.parts = parts
        self.meanPartSize = meanPartSize
        self.weightTaper = 0.0

    @final
    @overrides
    def schedule(self, steps):
        if self.meanPartSize is None:
            parts = self.parts
            if parts > steps:
                sys.stderr.write('Truncation is greater than the number of steps')
                parts = steps
        else:
            parts = max(int(steps / self.meanPartSize), 1)
        ps = N.linspace(0, steps, parts+1).astype(int)
        ws = 1.0 - N.linspace(self.weightTaper, 1, parts)
        ws /= ws.sum()
        return [Segment(ps[i], ps[i+1], w)
                for i,w in zip(range(ps.size - 1), ws)]

class TestBPTT(unittest.TestCase):

    @staticmethod
    def check_schedule(trunc, steps):
        schedule = SchedulerEven(trunc).schedule(steps)
        laststop = 0
        for seg in schedule:
            if seg.start != laststop:
                return False
            if seg.start >= seg.stop:
                return False
            laststop = seg.stop
        return laststop == steps


    def test_segment_equal(self):
        self.assertEqual(Segment(1, 5), Segment(1,5))
        self.assertNotEqual(Segment(1, 5), Segment(1,6))

    def test_truncated_bptt_schedule(self):

        self.assertTrue(self.check_schedule(5, 93))
        self.assertTrue(self.check_schedule(73, 73))

if __name__ == '__main__':
    unittest.main()
