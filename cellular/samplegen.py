import abc
import numpy as N
import numpy.random as NR
import cellular.model2d
from overrides import final, overrides

class Image:

    def __init__(self, name=None, array=None):
        self.name = name
        self.array = array

class Sample:
    
    def __init__(self, indices, x_src, y_true, meta={}):
        """

        indices: (list) Sample index for reference purposes
        y_true: (list) Ground truth
        x_src: (list) Source cell state

        meta: Dictionary of lists
            'maskLock': Value update masks
            'scales': Scales of perception filters
        """
        assert indices.ndim == 1

        self.indices = indices
        self.x_src = x_src
        self.y_true = y_true
        self.meta = meta

        assert len(self) == self.x_src.shape[0]
        assert len(self) == self.y_true.shape[0]
        
    def __len__(self):
        return self.indices.shape[0]

    def __getitem__(self, key):
        i_true = self.indices[key]
        y_true = self.y_true [key] if self.y_true is not None else [None] * len(key)
        x_src  = self.x_src  [key]
        meta   = {k:v[key] for k,v in self.meta.items()}

        return Sample(i_true, x_src, y_true, meta)

    def __setitem__(self, key, s):
        assert isinstance(s, Sample)
        #assert len(key) == len(s), \
        #        f"Length of key {len(key)} is incompatible with length of sample {len(sample)}"
        self.indices[key] = s.indices
        self.x_src  [key] = s.x_src
        self.y_true [key] = s.y_true
        for k in self.meta.keys():
            if k in s.meta:
                self.meta[k][key] = s.meta[k]


    
    
class SampleGen(metaclass=abc.ABCMeta):
    
    def __init__(self, cfg, rand=NR.RandomState()):
        self.cfg = cfg
        if not isinstance(rand, NR.RandomState):
            rand = NR.RandomState(rand)
        self.rand = rand
        pass
    
    @abc.abstractmethod
    def draw_seed_samples(self, k=1):
        pass
    
    
class SampleGenErode(SampleGen):
    """
    This class is used for training a repairing model.
    """
    
    def __init__(self, cfg, images,
                 rand=NR.RandomState(),
                 cycleSeedSample=True):
        """
        images: A list [(name, image)].
        """
        super().__init__(cfg, rand)
        
        assert isinstance(images, list)
        assert len(images) > 0
        if not isinstance(images[0], Image):
            images = [Image(n, i) for n,i in images]
        self.images = images
        self.cycleSeedSample = 0 if cycleSeedSample else None

    @final
    @overrides
    def draw_seed_samples(self, n=1):
        """
        Draw n >= 1 seed samples
        """
        assert n > 0, "n must be >= 1"

        if self.cycleSeedSample is not None:
            indices = [(i + self.cycleSeedSample)%len(self.images)
                       for i in range(n)]
            self.cycleSeedSample = (n + self.cycleSeedSample) % len(self.images)
        else:
            indices = self.rand.choice(range(len(self.images)), n, replace=True)
        indices = N.array(indices, int)
        assert indices.ndim == 1

        return self.seed_samples(indices)

    def seed_samples(self, indices):
        n = len(indices)
        y_true,meta = zip(*[self.preprocess_image(self.images[i]) for i in indices])
        
        def random_erode_image(i):
            y = y_true[i]
            assert y.ndim == 3

            mask = self.erode_sample(self.images[indices[i]].name, meta[i])
            assert mask.ndim == 2, "Mask must be 2-dimensional"
            x = self.add_substrata(y * mask[...,N.newaxis])
            return x, mask
        
        assert n == len(y_true)
        x_src, maskLock = zip(*[random_erode_image(i) for i in range(n)])
        

        def process_kth(k):
            li = [m[k] for m in meta]
            return N.stack(li)
        
        s = Sample(indices=indices,
                   x_src=N.stack(x_src),
                   y_true=N.stack(y_true), 
                   meta={
            'maskLock': N.stack(maskLock),
            **{k:process_kth(k) for k in meta[0].keys()}
        })
        assert s.indices.shape == (n,)
        assert s.x_src.shape  == (n, self.cfg.width, self.cfg.height, self.cfg.nChannels)
        assert s.y_true.shape == (n, self.cfg.width, self.cfg.height, 1+self.cfg.nVisible)
        assert s.meta['maskLock'].shape == (n, self.cfg.width, self.cfg.height)
        return s
    
    def random_align_image(self, image):
        assert image.ndim == 3
        width_in = image.shape[0]
        if width_in < self.cfg.width:
            # Generate random pad amount left and right
            pad_left = self.rand.randint(0, self.cfg.width - width_in + 1)
            pad_right = self.cfg.width - width_in - pad_left
            image = N.pad(image, ((pad_left, pad_right), (0,0), (0,0)))
            cornerW = -pad_left
        elif width_in > self.cfg.width:
            i_begin = self.rand.randint(0, width_in - self.cfg.width + 1)
            i_end = i_begin + self.cfg.width
            assert i_end <= width_in
            image = image[i_begin:i_end,:,:]
            cornerW = i_begin
        else:
            cornerW = 0

        height_in = image.shape[1]
        if height_in < self.cfg.height:
            pad_left = self.rand.randint(0, self.cfg.height - height_in + 1)
            pad_right = self.cfg.height - height_in - pad_left
            image = N.pad(image, ((0,0), (pad_left, pad_right), (0,0)))
            cornerH = -pad_left
        elif height_in > self.cfg.height:
            i_begin = self.rand.randint(0, height_in - self.cfg.height + 1)
            i_end = i_begin + self.cfg.height
            assert i_end <= height_in
            image = image[:,i_begin:i_end,:]
            cornerH = i_begin
        else:
            cornerH = 0

        assert image.shape == (self.cfg.width, self.cfg.height, 1+self.cfg.nVisible)
        return image, {'cornerW': cornerW, 'cornerH': cornerH}

    @abc.abstractmethod
    def erode_sample(self, name, meta):
        """
        Return a 2d mask of the same size as the output.
        """
        pass

    def preprocess_image(self, image):
        """
        Generate y and metadata dictionary corresponding to the image.
        """
        return image.array.copy(), {}
    
    def add_substrata(self, y):
        """
        Given a (width, height, 1+self.cfg.nVisible) tensor,
        return a (width, height, self.cfg.nChannels) tensor.

        By default, the padding is all 0's.
        """
        assert y.ndim == 3, "Invalid shape {}".format(y.shape)
        return N.pad(y, ((0,0), (0,0), (0, self.cfg.nSubstrata)))

    
    
