import os

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import numpy as N
import tensorflow as tf
import cellular.model2d as CM

import unittest

class TestModel2d(unittest.TestCase):

    def test_ties(self):

        cfg = CM.WorldConfig(width=5, height=5, dtype='float64')

        tie = N.array([0,0,1,1,2], int)
        world = N.array([
            [0,1,2,3,4],
            [1,2,3,4,5],
            [2,3,4,5,6],
            [3,4,5,6,7],
            [4,5,6,7,8],
        ], float)[N.newaxis,:,:,N.newaxis]

        # Tie over axis 0
        tie1 = tf.cast(cfg.preprocess_tie(tie, axis=0), tf.float64)
        self.assertEqual(tie1.shape, (5,5))

        world2 = cfg.apply_ties(world, tie1[N.newaxis,...,N.newaxis], None).numpy()
        self.assertEqual(world.shape, world2.shape)

        N.testing.assert_array_almost_equal(world2[0,:,:,0], N.array([
            [0.5,1.5,2.5,3.5,4.5],
            [0.5,1.5,2.5,3.5,4.5],
            [2.5,3.5,4.5,5.5,6.5],
            [2.5,3.5,4.5,5.5,6.5],
            [4,5,6,7,8],
        ], float))

        tie2 = tf.cast(cfg.preprocess_tie(tie, axis=0), tf.float64)
        self.assertEqual(tie2.shape, (5,5))
        world2 = cfg.apply_ties(world, None, tie2[N.newaxis,...,N.newaxis]).numpy()
        self.assertEqual(world.shape, world2.shape)

        N.testing.assert_array_almost_equal(world2[0,:,:,0], N.array([
            [0.5,0.5,2.5,2.5,4],
            [1.5,1.5,3.5,3.5,5],
            [2.5,2.5,4.5,4.5,6],
            [3.5,3.5,5.5,5.5,7],
            [4.5,4.5,6.5,6.5,8],
        ], float))

    def test_stripes(self):
        t1, t2 = CM.WorldConfig.create_stripes(13, 6, 1)
        self.assertEqual(t1.shape, (13,))
        self.assertEqual(t2.shape, (13,))
        self.assertEqual(list(t1),
            [1, 2, 3, 4, 5, 0, 1, 2, 3, 4, 5, 0, 1]
        )
        self.assertEqual(list(t2),
            [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2]
        )

        t1, t2 = CM.WorldConfig.create_stripes(13, 6, -1)
        self.assertEqual(t1.shape, (13,))
        self.assertEqual(t2.shape, (13,))
        self.assertEqual(list(t1),
            [5, 0, 1, 2, 3, 4, 5, 0, 1, 2, 3, 4, 5]
        )
        self.assertEqual(list(t2),
            [0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2]
        )

        for i in range(32):
            t1, t2 = CM.WorldConfig.create_stripes(32, 6, i)
            self.assertEqual(t1.shape, (32,))
            self.assertEqual(t2.shape, (32,))

if __name__ == '__main__':
    unittest.main()
