import numpy as N
import numpy.random as NR
import pandas as P

import tensorflow as tf
import tensorflow.keras as K
import tensorflow.keras.optimizers as KO

import cellular.bptt
import cellular.losses
import cellular.samplegen

class Trainer:
    
    def __init__(self,
                 model,
                 sampleGen,
                 loss      = cellular.losses.l2_loss,
                 metric    = None,
                 rand      = NR.RandomState(),
                 optimiser = KO.Adam(),
                 samplePoolSize = 256,
                 samplePoolRandom = False,
                 timeRange      = range(64, 96),
                 schedulerBPTT  = cellular.bptt.SchedulerEven(),
                 epsGradient    = None,
                 normGradient   = True):

        self.model = model
        assert isinstance(self.model, cellular.model2d.CAModel), \
               "Invalid model"
        self.cfg = model.cfg
        self.sampleGen = sampleGen
        if not isinstance(rand, NR.RandomState):
            rand = NR.RandomState(rand)
        self.rand = rand

        self.loss = loss
        assert callable(self.loss), "Invalid loss function"
        if isinstance(metric, dict):
            metric = lambda s,x: {k:vf(s,x) for k,vf in metric.items()}
        assert metric is None or callable(metric)
        self.metric = metric
        self.optimiser = optimiser

        self.samplePoolSize = samplePoolSize
        self.samplePoolRandom = samplePoolRandom
        self.timeRange = timeRange
        self.schedulerBPTT = schedulerBPTT

        if epsGradient is None:
            if self.cfg.dtype == tf.float16:
                self.epsGradient = 1e-5
            else:
                self.epsGradient = 1e-8
        else:
            self.epsGradient = epsGradient
        self.normGradient = normGradient

        self._pool = None
        self._poolShift = 0
        
        
    def eval_loss(self, s, y_true, x_pred):
        #y_true = self.cfg.to_visible(x_true)
        #y_pred = self.cfg.to_visible(x_pred)
        #assert y_true.shape == y_pred.shape, \
        #       f"Incompatible shape: {y_true.shape}, {y_pred.shape}"
        #assert y_pred.shape[-1] == self.cfg.nVisible + 1
        result = self.loss(s, y_true, x_pred)
        assert result.ndim == 1
        assert result.shape[0] == len(s)
        return result
    
    def train_step(self, s, y_true, x):
        assert y_true.ndim == 4
        assert y_true.shape[-1] == self.cfg.nVisible + 1
        assert x.shape[-1] == self.cfg.nChannels
        assert y_true.shape[0] == x.shape[0]
        assert x.shape[1:] == self.cfg.shape
        
        iters = self.timeRange[self.rand.randint(0, len(self.timeRange))]
        #meta = {k:tf.convert_to_tensor(v) if v.dtype != 'O' else v
        #        for k,v in s.meta.items()}

        meta = {k:self.cfg.rec_cast(v) for k,v in s.meta.items()}

        losses = []
        gradients = None
        for segment in self.schedulerBPTT.schedule(iters):
            with tf.GradientTape() as tape:
                x = self.model.execute(x, segment.to_tensor(), meta, training=True)
                loss = tf.reduce_mean(self.eval_loss(s, y_true, x))
                     #+ tf.reduce_sum(self.model.dmodel.losses)
            grad = tape.gradient(loss, self.model.weights)
            losses.append(loss * segment.weight)

            assert len(grad) == len(self.model.weights)

            if self.normGradient:
                grad = [g/(tf.norm(g) + self.epsGradient) for g in grad]

            if gradients is None:
                gradients = grad
            else:
                for i,g in enumerate(grad):
                    gradients[i] += g * segment.weight

        #self.optimiser.apply_gradients(zip(gradients, self.model.trainable_variables))
        self.optimiser.apply_gradients(zip(gradients, self.model.weights))

        return x, tf.reduce_mean(losses)
    
    def train(self,
            initial_step=0, steps=100,
            batchSize=8, restartSize=2, restartBest=0,
            verbose=None, showLogLoss=True, plotVariables=[],
            logInterval=50, callback=None):
        """
        Train the model from step {initial_step}+1 to {steps}
        In each step, draw {batchSize} samples from the sample pool and
        replace the {restartSize} with the highest losses with new seed
        samples.

        If restartBest is true, restart on samples with the lowest loss as well
        to avoid overfitting.
        """
        assert 1 <= batchSize, "Invalid batch size %d" % batchSize
        assert 0 <= restartSize and restartSize <= batchSize, \
                "Restart size must be in [0 .. batchSize]"

        hist = P.DataFrame(columns=['step', 'loss'])

        if verbose:
            print("Drawing {} seed samples ...".format(self.samplePoolSize))
        if self._pool is None:
            # Re-generate all seed samples.
            self._pool = self.sampleGen.draw_seed_samples(self.samplePoolSize)
            self._poolShift = 0
        assert len(self._pool) == self.samplePoolSize

        for step in range(initial_step+1, steps+1):
            # Draw sample from existing pool
            s = self.draw_sample(batchSize)
            # Reverse sort the sample indices by loss
            loss_rank = self.eval_loss(s, s.y_true, s.x_src).numpy().argsort()[::-1]
            
            # Replace the sample of the highest loss by another seed sample
            # to prevent catastrophic forgetting.

            s = s[loss_rank]
            assert len(s) == batchSize, \
                   "Ordering the sample by loss rank has changed its size"

            # draw new seed samples
            if restartSize > 0:
                s2 = self.sampleGen.draw_seed_samples(restartSize)
                s[:restartSize] = s2
            if restartBest > 0:
                s2 = self.sampleGen.draw_seed_samples(restartBest)
                s[-restartBest:] = s2
            
            y_true = tf.cast(s.y_true, self.cfg.dtype)
            x_src  = tf.cast(s.x_src,  self.cfg.dtype)
            x, loss = self.train_step(s, y_true, x_src)
            
            loss = loss.numpy()
            if showLogLoss:
                loss = N.log10(loss)

            met = {} if self.metric is None else self.metric(s, x)
            hist = hist.append({
                'step': step,
                'loss': loss,
                **met
            }, ignore_index=True)

            if callback: callback(step, hist)
            
            # Print diagnostics
            if verbose and step%logInterval == 0:
                import IPython.display
                import matplotlib.pyplot as pyplot

                IPython.display.clear_output()
                if callable(verbose): verbose(s, x)

                fig,ax = pyplot.subplots(figsize=(10,4))
                ax.set_title('Loss history')
                ax.plot(hist['step'], hist['loss'], '+k', alpha=0.1, label='loss')
                ax.set_xlabel('Steps')
                if len(plotVariables) > 0:
                    ax2 = ax.twinx()
                    for v in plotVariables:
                        ax2.plot(hist['step'], hist[v], '.', alpha=0.1, label=v)
                    ax2.grid(False)
                    fig.legend(loc="upper right")
                #fig.show()
                pyplot.show()

                #pyplot.figure(figsize=(10, 4))
                #pyplot.title('Loss history')
                #pyplot.plot(hist['loss'], '.', alpha=0.1)
                #pyplot.xlabel('Steps')
                #pyplot.show()
        
            logstr = '\r step: %d/%d, loss: %.3f'%(step, steps, loss) + \
                    ''.join(', %s: %.3f' % (k, v) for k,v in met.items()) + \
                    '   '
            print(logstr, end='')

            if N.isnan(loss):
                raise Exception('Loss is nan')

            # Update and commit the sample.
            s.x_src = x.numpy()
            self.commit_sample(s)
            
        return hist
            
    # Sampling functions
    def draw_sample(self, k=1):
        assert self._pool.indices is not None
        
        draw = self.rand.randint(0, len(self._pool.indices), size=k)
        return self._pool[draw]
    
    def commit_sample(self, s):
        nNew = len(s.indices)
        if self.samplePoolRandom:
            replIndex = self.rand.choice(range(len(self._pool.indices)), nNew, replace=False)
            replIndex = list(replIndex)
        else:
            replIndex = [(i + self._poolShift)%len(self._pool.indices)
                          for i in range(nNew)]
            self._poolShift = (nNew + self._poolShift)%len(self._pool.indices)
        self._pool[replIndex] = s
    
